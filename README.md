# Kouba  |  工場 

## Industrial pentesting



**Introduction**

There are tons of open toold for industrial security and pentesting, this is aimed to be another repository for summing them up. The only difference is in this repo it's included a VM for pentesting and **researching**: **Kouba**. 

![](https://gitlab.com/terceranexus6/kouba/-/raw/master/images/img2.jpg)

Kouba is a cute, Debian 10 based VM for industrial pentesting and footprinting. These are some of the specifications:

* xfce4 terminal
* Cinammon
* nmap with REDPOINT
* Kamerka (requires Shodan key)
* s7scan
* Aztarna
* PLCinject
* Metasploit with Basecamp scripts

## INDEX

* [OVAS](https://mega.nz/folder/adUxlQhT)
* [Arduino SETUP and notes](https://gitlab.com/terceranexus6/kouba/-/wikis/Tips-for-Arduino)
* [Scripts for installing on your machine](https://gitlab.com/terceranexus6/kouba/scripts)
* [Cheatsheets](https://gitlab.com/terceranexus6/kouba/-/wikis/CHEATSHEETS)

Example of Kouba default Desktop with all the tools:
![](https://gitlab.com/terceranexus6/kouba/-/raw/master/images/im1.gif)

## DEPRECATED: Kamerka has been removed in the latest versions due to its recent deprecation
You need to change `keys.json` in Kamerka and add the Shodan API key. All the requirements are installed but you need to launch celery to make it work in http://localhost:8000/, check the Wiki for more information.

## LOGIN

Root login:

old version:
root:Kn1f3_K1tty

new version:
root:Ch4ng3_m3


User login:

old version:
peasant:F0rky_K1tty

new version:
normal:Ch4ng3_m3

Please note the system default keyboard is **American**, and the inside one is Spanish. You can change that!


