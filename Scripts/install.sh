#!/bin/bash


# creates the directories
mkdir Enumeration Exploits Worshop 
cd Exploits ; mkdir Modules
cd ..
cd Workshop; mkdir Lab Resources



# Install neovim, comment if you enjoy vim or... the others

apt install neovim

# Install some basics...

apt install python-pip python3-pip git wormhole curl wget nmap gem rubygems
pip3 install docker

# Installing aztarna for robots, also comes with scapy
pip3 install aztarna 

#Installing snap7, a python wrapper for interfacing natively with Siemens S7 PLCs
# for more info on this, please check the wiki :)
pip3 install  python-snap7


cd Exploits

	# Installing msfconsole
	sudo apt install postgresql postgresql-contrib
	curl https://raw.githubusercontent.com/rapid7/metasploit-omnibus/master/config/templates/metasploit-framework-wrappers/msfupdate.erb > msfinstall
	chmod 755 msfinstall
	./msfinstall

	# Installing ISF (similar to msf) uses python 2.7 >:(
	git clone https://github.com/w3h/isf.git
	# the last two requirements are for windows so... wiping it
	cd isf

		#This next step is theorically unnecessary, I know, just in case
		rm requirements.txt
##
		echo -e "gnureadline\npycrypto" > requirements.txt
	pip install -r requirements.txt

	cd ..

	cd Modules

		# Wanna add 'em? execute install_modules.sh
		git clone https://github.com/jpalanco/nmap-scada.git
		git clone https://github.com/digitalbond/Basecamp.git

cd ..

cd Workshop
cd Resources

	# perl for modbus
	git clone https://github.com/sourceperl/mbtget.git
	
	# On-chip debugging
	git clone https://github.com/riscv/riscv-openocd.git

	# multifunction interaction with raw serial
	git clone https://github.com/ControlThingsTools/ctserial.git
	cd ctserial
		pip3 install -r requirements.txt
	cd ..

	# same with modbus devices
	git clone https://github.com/ControlThingsTools/ctmodbus.git
	cd ctmodbus
		pip3 install -r requirements.txt
	cd ..

	# analyzing binary files
	git clone https://github.com/ControlThingsTools/ctbin.git

	# modbus ruby 
	git clone https://github.com/tallakt/modbus-cli.git
	gem install modbus-cli

cd ..

cd Lab

	# Creating terminal-based interfaces
	git clone https://github.com/ControlThingsTools/ctui.git

	# A lab of defcon made by Arnaud Soullie that I think is very usefull!
	git clone https://github.com/arnaudsoullie/funwithmodbus0x5a.git

	# Installing pyserial for the next tool
	pip install pyserial

	# Interacting with Velocip Ace PLC via USB
	git clone https://github.com/ControlThingsTools/ctvelocio.git
	
cd ..

